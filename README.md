# Manon Graham website

https://manongraham.chades.fr


# Technologies used
I used PHP (and apache) and followed MVC conventions.  
I used Docker and Docker Compose to create my images and embed them easily on any configuration.  

# Screenshots

## Some user interface
<div align=center>
    <p>Home page</p>
    <img src="assets/img/home.png" alt="home" height="500"/>
    <p>Biography page</p>
    <img src="assets/img/bio.png" alt="home"/>
    <p>Portfolio page</p>
    <img src="assets/img/portfolio.png" alt="home"/>
</div>

## Some admin interface
<div align=center>
    <p>Admin home page</p>
    <img src="assets/img/admin.png" alt="home" height="500"/>
    <p>Admin contact page</p>
    <img src="assets/img/contact.png" alt="home"/>
    <p>Admin creation form page</p>
    <img src="assets/img/creation.png" alt="home"/>
</div>

# Installation
0. Make shure you have installed docker, docker compose  
> https://docs.docker.com/engine/install/ubuntu/  
> https://docs.docker.com/compose/install/
1. Clone this repository and move into it.  
```shell
git clone git@gitlab.com:ChadOW/manongraham-website.git
cd manongraham-website/
```
2. Create a folder "uploads" in ./php/ and give full permission to folder "uploads".  
```shell
mkdir php/uploads
chmod 777 php/uploads/
```
3. Create "config" folder based on "config_ex" folder and edit configurations with your database credentials and your admin hashed password.  
```shell
cp config_ex/ config/ -r
nano config/DBConf.php
nano config/GConf.php
```
4. Create ".env" file based on ".env_ex" file and edit configuration with credentials your want.  
```shell
cp .env_ex .env
nano .env
```
5. create docker image of php-apache with PDO and launch your docker.
```shell
sudo docker build -t phppdo:1.0 .
sudo docker-compose up -d
```
Now your website is accessible on:
> < local IP >:8081  

You can use a reverse proxy like nginx or traefik to configure domain name and ssl encryption. 
