<?php
require_once("model/Model.php");
class ModelContact implements Model {

    private $id;
    private $name;
    private $mail;
    private $object;
    private $message;
    private $date;
    private $archived;

    /**
     * @param $id
     * @param $name
     * @param $mail
     * @param $object
     * @param $message
     * @param $date
     */
    public function __construct($id, $name, $mail, $object, $message, $date, $archived)
    {
        $this->id = $id;
        $this->setName($name);
        $this->setMail($mail);
        $this->setObject($object);
        $this->setMessage($message);
        $this->setDate($date);
        $this->setArchived($archived);
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param mixed $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }






    private static function createOneCreationWithPDO($pdo)
    {
        $model = new ModelContact($pdo->id, $pdo->name, $pdo->mail, $pdo->object, $pdo->message, $pdo->date, $pdo->archived);
        return $model;
    }

    private static function createCreationsWithPDO($array)
    {
        $tab = array();
        foreach ($array as $v)
            $tab[] = self::createOneCreationWithPDO($v);
        return $tab;
    }


    public static function getByID($id)
    {
        return self::getByAttribute('id', $id);
    }

    public static function getByAttribute($attribut, $value)
    {
        require_once('assets/utils/RequestBuilder.php');
        $rq = new RequestBuilder('Contact', array("id", "name", "mail", "object", "message", "date", "archived"));
        $rq->setCondition($attribut, $value, false);
        $rq = $rq->fetch(null);
        if ($rq != false) {
            return self::createOneCreationWithPDO($rq[0]);
        }
        return false;
    }

    public static function getAll()
    {
        require_once('assets/utils/RequestBuilder.php');
        $rq = new RequestBuilder('Contact', array("id", "name", "mail", "object", "message", "date", "archived"));
        $rq->orderBy('date DESC');
        return self::createCreationsWithPDO($rq->fetch(null));
    }

    public static function getAllByAttribute($attribut, $value)
    {
        require_once('assets/utils/RequestBuilder.php');
        $rq = new RequestBuilder('Contact', array("id", "name", "mail", "object", "message", "date", "archived"));
        $rq->setCondition($attribut, $value, false);
        $rq->orderBy('date DESC');
        return self::createCreationsWithPDO($rq->fetch(null));
    }

    public function delete()
    {
        $sql = "DELETE FROM Contact WHERE id = :id";
        try {
            $req_prep = DBCom::getPDO()->prepare($sql);
            $values = array("id" => $this->id);
            $req_prep->execute($values);
            return true;
        } catch(PDOException $e) {
            require_once('assets/utils/CustomError.php');
            CustomError::callError($e->getMessage());
            return false;
        }
    }

    public function save()
    {
        if (self::getByID($this->getId()) == false) {
            $sql = "INSERT INTO Contact (name, mail, object, message, date, archived) VALUES (:name, :mail, :object, :message, :date, :archived)";
            try {
                $req_prep = DBCom::getPDO()->prepare($sql);
                $values = array(
                    "name" => $this->name,
                    "mail" => $this->mail,
                    "object" => $this->object,
                    "message" => $this->message,
                    "date" => $this->date,
                    "archived" => $this->archived);
                $req_prep->execute($values);
                return true;
            } catch (PDOException $e) {
                require_once('assets/utils/CustomError.php');
                CustomError::callError($e->getMessage());
                return false;
            }
        } else {
            $sql = "UPDATE Contact SET name = :name, mail = :mail, object = :object, message = :message, date = :date, archived = :archived WHERE id = :id";
            try {
                $req_prep = DBCom::getPDO()->prepare($sql);
                $values = array(
                    "name" => $this->name,
                    "mail" => $this->mail,
                    "object" => $this->object,
                    "message" => $this->message,
                    "date" => $this->date,
                    "archived" => $this->archived,
                    "id" => $this->id);
                $req_prep->execute($values);
                return true;
            } catch (PDOException $e) {
                require_once('assets/utils/CustomError.php');
                CustomError::callError($e->getMessage());
                return false;
            }
        }
    }
}