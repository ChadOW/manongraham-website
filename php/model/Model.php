<?php
interface Model {

    public static function getByID($id);
    public static function getByAttribute($attribut, $value);

    public static function getAll();
    public static function getAllByAttribute($attribut, $value);

    public function delete();
    public function save();

}
