<?php
require_once("model/Model.php");
class ModelCreation implements Model {

    private $id;
    private $name;
    private $description;
    private $date;
    private $isFavorite;
    private $isVisible;
    private $files;


    public function __construct($id, $name, $description, $date, $isFavorite, $isVisible) {
        $this->files = array();
        $this->setId($id);
        $this->setName($name);
        $this->setDescription($description);
        $this->setDate($date);
        $this->setIsFavorite($isFavorite);
        $this->setIsVisible($isVisible);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getIsFavorite()
    {
        return $this->isFavorite;
    }

    /**
     * @param mixed $isFavorite
     */
    public function setIsFavorite($isFavorite)
    {
        $this->isFavorite = $isFavorite;
    }

    /**
     * @return mixed
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param mixed $isVisible
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }





    private static function createOneCreationWithPDO($pdo)
    {
        $model = new ModelCreation($pdo->id, $pdo->name, $pdo->description, $pdo->date, $pdo->isfavorite, $pdo->isvisible);
        $model->files = json_decode($pdo->files);
        return $model;
    }
    private static function createCreationsWithPDO($array)
    {
        $tab = array();
        foreach ($array as $v)
            $tab[] = self::createOneCreationWithPDO($v);
        return $tab;
    }


    public static function getByID($id)
    {
        return self::getByAttribute('id', $id);
    }

    public static function getByAttribute($attribut, $value)
    {
        require_once('assets/utils/RequestBuilder.php');
        $rq = new RequestBuilder('Creation', array("id", "name", "description", "date", "isfavorite", "isvisible", "files"));
        $rq->setCondition($attribut, $value, false);
        $rq = $rq->fetch(null);
        if ($rq != false) {
            return self::createOneCreationWithPDO($rq[0]);
        }
        return false;
    }

    public static function getAll()
    {
        require_once('assets/utils/RequestBuilder.php');
        $rq = new RequestBuilder('Creation', array("id", "name", "description", "date", "isfavorite", "isvisible", "files"));
        $rq->orderBy('isfavorite DESC, date DESC');
        return self::createCreationsWithPDO($rq->fetch(null));
    }

    public static function getAllByAttribute($attribut, $value)
    {
        require_once('assets/utils/RequestBuilder.php');
        $rq = new RequestBuilder('Creation', array("id", "name", "description", "date", "isfavorite", "isvisible", "files"));
        $rq->setCondition($attribut, $value, false);
        $rq->orderBy('isfavorite DESC, date DESC');
        return self::createCreationsWithPDO($rq->fetch(null));
    }

    public function delete()
    {
        $this->deleteFiles();
        $sql = "DELETE FROM Creation WHERE id = :id";
        try {
            $req_prep = DBCom::getPDO()->prepare($sql);
            $values = array("id" => $this->id);
            $req_prep->execute($values);
            return true;
        } catch(PDOException $e) {
            require_once('assets/utils/CustomError.php');
            CustomError::callError($e->getMessage());
            return false;
        }
    }

    public function save()
    {
        if (self::getByID($this->getId()) == false) {
            $sql = "INSERT INTO Creation (name, description, date, isfavorite, isvisible, files) VALUES (:name, :desc, :date, :isf, :isv, :files)";
            try {
                $req_prep = DBCom::getPDO()->prepare($sql);
                $values = array(
                    "name" => $this->name,
                    "desc" => $this->description,
                    "date" => $this->date,
                    "isf" => $this->isFavorite,
                    "isv" => $this->isVisible,
                    "files" => json_encode($this->files));
                $req_prep->execute($values);
                return true;
            } catch (PDOException $e) {
                require_once('assets/utils/CustomError.php');
                CustomError::callError($e->getMessage());
                return false;
            }
        } else {
            $sql = "UPDATE Creation SET name = :name, description = :desc, date = :date, isfavorite = :isf, isvisible = :isv, files = :files WHERE id = :id";
            try {
                $req_prep = DBCom::getPDO()->prepare($sql);
                $values = array(
                    "name" => $this->name,
                    "desc" => $this->description,
                    "date" => $this->date,
                    "isf" => $this->isFavorite,
                    "isv" => $this->isVisible,
                    "files" => json_encode($this->files),
                    "id" => $this->id);
                $req_prep->execute($values);
                return true;
            } catch (PDOException $e) {
                require_once('assets/utils/CustomError.php');
                CustomError::callError($e->getMessage());
                return false;
            }
        }
    }

    public function setFilesFromFILES()
    {
        $total = strlen($_FILES['upload']['name'][0]);
        if ($total > 0) {
            $this->deleteFiles();

            for( $i=0 ; $i < $total ; $i++ ) {

                $tmpFilePath = $_FILES['upload']['tmp_name'][$i];
                if ($tmpFilePath != ""){
                    //Setup our new file path
                    $fileExt = explode('.', $_FILES['upload']['name'][$i]);
                    $newFilePath = "uploads/" . uniqid('', true) . '.' . strtolower(end($fileExt));

                    //Upload the file into the temp dir
                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                        $this->files[] = $newFilePath;
                    }
                }
            }
        }
    }

    public function deleteFiles() {
        foreach ($this->files as $path) {
            unlink($path);
        }
        $this->files = array();
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    public function getFirstFileHTML() {
        if (sizeof($this->getFiles()) > 0) {
            $path = $this->getFiles()[0];
            $explode = explode('.', $path);
            $end = end($explode);
            if (strcasecmp($end, "png") == 0 or strcasecmp($end, "jpg") == 0 or strcasecmp($end, "jpeg") == 0 or strcasecmp($end, "svg") == 0 or strcasecmp($end, "gif") == 0) {
                return '<img src="' . $path . '" />';
            } else if (strcasecmp($end, "mp4") == 0 or strcasecmp($end, "webm") == 0 or strcasecmp($end, "ogg") == 0) {
                return '<video controls><source src="' . $path . '" type="video/mp4"> Sorry, your browser doesn t support embedded videos.</video>';
            } else {
                return '<a class="file" href="' . $path . '" download="file.' . $end . '"><span>Download</span><span>' . $end . '</span></a>';
            }
        } else {
            return false;
        }
    }

    public function getFilesHTML() {
        $tab = array();
        foreach (self::getFiles() as $path) {
            $explode = explode('.', $path);
            $end = end($explode);
            if (strcasecmp($end, "png") == 0 or strcasecmp($end, "jpg") == 0 or strcasecmp($end, "jpeg") == 0 or strcasecmp($end, "svg") == 0 or strcasecmp($end, "gif") == 0) {
                $tab[] = '<img src="' . $path . '" />';
            } else if (strcasecmp($end, "mp4") == 0 ) {
                $tab[] = '<video controls><source src="' . $path . '" type="video/mp4"> Sorry, your browser doesn t support embedded videos.</video>';
            } else {
                $tab[] = '<a class="file" href="' . $path . '" download="file.' . $end . '"><span>Download</span><span>' . $end . '</span></a>';
            }
        }
        return $tab;
    }


}