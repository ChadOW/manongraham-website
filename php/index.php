<?php
    session_start();
    error_reporting(E_ERROR | E_PARSE);

    if (sizeof($_GET) > 0) {
        if (array_key_exists("p", $_GET)) {
            $p = $_GET["p"];

            if (file_exists("controller/" . $p . ".php")) {
                require_once("controller/" . $p . ".php");

                if (array_key_exists("f", $_GET)) {
                    $f = $_GET["f"];
                    if (method_exists($p, $f))
                        $p::$f();
                    else {
                        require_once("assets/utils/CustomError.php");
                        CustomError::callError("La fonction " . $f . " du controlleur " . $p . " n'existe pas");
                    }
                } else {
                    $p::main();
                }
            } else {
                require_once("assets/utils/CustomError.php");
                CustomError::callError("Le controlleur " . $p . " n'existe pas");
            }
        } else {
            require_once("assets/utils/CustomError.php");
            CustomError::callError("Paramètres GET inconnus");
        }
    } else {
        $controller = 'global';
        $view = 'home';
        require_once('view/view.php');
    }

