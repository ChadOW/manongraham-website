<?php

require_once('controller/Controller.php');
require_once('assets/utils/User.php');
class Administrator implements Controller {

    public static function main()
    {
        if (User::isConnected()) {
            $controller = 'admin';
            $view = 'home';
            require_once('view/view.php');
        } else {
            header('location: ./?p=Administrator&f=denied');
        }
    }

    public static function contactlist() {
        if (User::isConnected()) {
            $controller = 'admin';
            $view = 'contacts';
            require_once('view/view.php');
        } else {
            header('location: ./?p=Administrator&f=denied');
        }
    }

    public static function archive() {
        if (User::isConnected()) {
            require_once('model/ModelContact.php');
            if (array_key_exists('id', $_GET)) {
                $v = ModelContact::getByID($_GET['id']);
                if ($v != false) {
                    if ($v->getArchived() == "1") {
                        $v->setArchived(0);
                    } else {
                        $v->setArchived(1);
                    }
                    $v->save();
                }
            }
            header('location: ./?p=Administrator&f=contactlist&archived=false');
        } else {
            header('location: ./?p=Administrator&f=denied');
        }
    }

    public static function login() {
        User::connect();
    }

    public static function disconnect() {
        User::disconnect();
    }


    public static function favorite() {
        if (User::isConnected()) {
            require_once('model/ModelCreation.php');
            if (array_key_exists('id', $_GET)) {
                $v = ModelCreation::getByID($_GET['id']);
                if ($v != false) {
                    if ($v->getIsFavorite() == 0)
                        $v->setIsFavorite(1);
                    else
                        $v->setIsFavorite(0);
                    $v->save();
                }
            }
            header('location: ./?p=Administrator');
        } else {
            header('location: ./?p=Administrator&f=denied');
        }
    }

    public static function visible() {
        if (User::isConnected()) {
            require_once('model/ModelCreation.php');
            if (array_key_exists('id', $_GET)) {
                $v = ModelCreation::getByID($_GET['id']);
                if ($v != false) {
                    if ($v->getIsVisible() == 0)
                        $v->setIsVisible(1);
                    else
                        $v->setIsVisible(0);
                    $v->save();
                }
            }
            header('location: ./?p=Administrator');
        } else {
            header('location: ./?p=Administrator&f=denied');
        }
    }

    public static function delete() {
        if (User::isConnected()) {
            require_once('model/ModelCreation.php');
            if (array_key_exists('id', $_GET)) {
                $v = ModelCreation::getByID($_GET['id']);
                if ($v != false) {
                    $v->delete();
                }
            }
            header('location: ./?p=Administrator');
        } else {
            header('location: ./?p=Administrator&f=denied');
        }
    }

    public static function updated() {
        if (User::isConnected()) {
            require_once('model/ModelCreation.php');
            if (array_key_exists('id', $_GET)) {
                $v = ModelCreation::getByID($_GET['id']);
                if ($v != false) {
                    $v->setName($_POST["name"]);
                    $v->setDescription($_POST["description"]);
                    $v->setDate($_POST["date"]);
                    $v->setFilesFromFILES();
                    $v->save();
                }
            }
            header('location: ./?p=Administrator');
        } else {
            header('location: ./?p=Administrator&f=denied');
        }
    }

    public static function created() {
        if (User::isConnected()) {
            require_once('model/ModelCreation.php');
            $v = new ModelCreation(null, $_POST["name"], $_POST["description"], $_POST["date"], 0, 1);
            $v->setFilesFromFILES();
            $v->save();
            header('location: ./?p=Administrator');
        } else {
            header('location: ./?p=Administrator&f=denied');
        }
    }

    public static function form() {
        if (User::isConnected()) {
            require("model/ModelCreation.php");
            $v = null;
            if(array_key_exists('id', $_GET)){
                $v = ModelCreation::getByID($_GET['id']);
                if($v == false){
                    require_once('assets/utils/CustomError.php');
                    CustomError::callError("Cet echantillon n'existe pas");
                }
            }

            $controller = 'admin';
            $view = 'form';
            require_once('view/view.php');
        } else {
            header('location: ./?p=Administrator&f=denied');
        }
    }

    public static function denied() {
        $controller = 'admin';
        $view = 'login';
        require_once('view/view.php');
    }
}