<?php

require_once('controller/Controller.php');
require_once('assets/utils/User.php');
class Visitor implements Controller {

    public static function contactconfirm() {
        $controller = 'visitor';
        $view = 'contactsave';
        require_once('view/view.php');
    }

    public static function portfolio() {
        $controller = 'visitor';
        $view = 'portfolio';
        require_once('view/view.php');
    }

    public static function about() {
        $controller = 'visitor';
        $view = 'about';
        require_once('view/view.php');
    }

    public static function contact() {
        $controller = 'visitor';
        $view = 'contact';
        require_once('view/view.php');
    }

    public static function contactcreated() {
        if (array_key_exists("name", $_POST) and array_key_exists("mail", $_POST) and array_key_exists("object", $_POST) and array_key_exists("message", $_POST)) {
            date_default_timezone_set('UTC');
            require_once('model/ModelContact.php');
            $v = new ModelContact(null, $_POST["name"], $_POST["mail"], $_POST["object"], $_POST["message"], date('Y-m-d'), 0);
            $v->save();
            header('location: ./?p=Visitor&f=contactconfirm');
        } else{
            header('location: ./?p=Visitor&f=contact');
        }
    }

    public static function see() {
        require_once('model/ModelCreation.php');
        if (array_key_exists('id', $_GET)) {
            $v = ModelCreation::getByID($_GET["id"]);
            if ($v != false) {
                require_once('assets/utils/User.php');
                if ($v->getIsVisible() == 0 && !User::isConnected()) {
                    require_once('assets/utils/CustomError.php');
                    CustomError::callError("Création non disponible au publique");
                }
                $controller = 'visitor';
                $view = 'see';
                require_once('view/view.php');
            } else {
                require_once('assets/utils/CustomError.php');
                CustomError::callError("Aucune création trouvée");
            }
        } else {
            require_once('assets/utils/CustomError.php');
            CustomError::callError("Aucune création précisée");
        }
    }

    public static function main()
    {
        header("location: ./");
    }
}