<div class="center">
    <div class="list center">
        <?php
        require_once('model/ModelCreation.php');
        foreach (ModelCreation::getAll() as $v) {
            if ($v->getIsVisible() == 1)
                echo '
                <a href="./?p=Visitor&f=see&id=' . $v->getId() . '"><div class="card">
                    <div class="top">
                        <div class="center"><h1>' . $v->getName() . '</h1></div>
                        <hr/>
                        <h2>Date: ' . $v->getDate() . '</h2>
                        <h2>Description: ' . $v->getDescription() . '</h2>
                    </div>
                    ' . $v->getFirstFileHTML() . '
                </div></a>';
        }
        ?>
    </div>
</div>