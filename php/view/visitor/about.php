<div class="center about">
    <img src="assets/img/me.png" alt="me"/>
    <div>
        <p>
            Actually a student of E-artsup based in Montpellier, I developed some affection for modeling and animating 3D objects during the first year. I also like 2D animation nonetheless.<br/><br/>
            At first I didn’t really know what I was doing here, wondering if I was in the right orientation, turned out I was exactly where I needed to be. This year was like a revelation, I never touched 3D nor 2D animation before this school, but then I just loved it.<br/><br/>
            I never knew what I wanted, now I hope at the end of school I’ll work in a studio of animation.
        </p>
    </div>
</div>