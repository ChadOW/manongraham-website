<div class="formulaire">
    <div class="container">
        <div class="titleform">
            <h1>Contact me</h1>
            <hr/>
        </div>
        <form method="post" action="./?p=Visitor&f=contactcreated" enctype="multipart/form-data">

            <div class="inline">
                <div class="input-group">
                    <label>Name</label>
                    <input type="text" placeholder="Enter your name" class="input-box" name="name" required>
                </div>

                <div class="input-group">
                    <label>E-Mail</label>
                    <input type="email" placeholder="Enter your e-mail" class="input-box" name="mail" required>
                </div>
            </div>

            <div class="input-group">
                <label>Object</label>
                <input type="text" placeholder="Enter message's object" class="input-box" name="object" required>
            </div>

            <div class="input-group">
                <label>Message</label>
                <textarea style="resize: vertical;" placeholder="Enter your message" class="input-box desc" name="message" required></textarea>
            </div>

            <p style="text-align: center">I'll process your message as soon as possible<br/>I'll come back to you with your email.</p>

            <div class="input-group">
                <button type="submit" name="submit" class="submit-btn">Envoyer</button>
            </div>

            <p style="text-align: center">Or contact me here:</p>

            <div class="inline">
                <a href="mailto:manon.graham.f@gmail.com"><img src="assets/img/Gmail-Logo.png" alt="GMail"></a>
                <a href="https://www.instagram.com/manon_grhm/"><img src="assets/img/instagram-logo.png" alt="Instagram"></a>
                <a href="https://discord.com/users/373372144836542466/"><img src="assets/img/discord-logo.png" alt="Discord"></a>
            </div>

        </form>
    </div>
</div>