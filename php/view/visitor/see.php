<div class="title center">
    <h1><?php echo $v->getName()?></h1><p>fait le <?php echo $v->getDate()?></p>
</div>
<hr/>
<div class="description center">
    <h2><?php echo nl2br($v->getDescription())?></h2>
</div>

<div class="carousel" data-flickity='{ "imagesLoaded": true, "percentPosition": false, "autoPlay": 3000 }'>
    <?php
    foreach ($v->getFilesHTML() as $html)
        echo '  <div class="carousel-cell">
                        ' . $html . '
                    </div>';
    ?>
</div>