
<div class="center">

    <?php
    require_once('model/ModelContact.php');

    $tab = array();
    if ($_GET["archived"] == "true") {
        echo '<a class="button" href="./?p=Administrator&f=contactlist&archived=false">Voir les messages non archivés</a>';
        $tab = ModelContact::getAllByAttribute("archived", "1");
    } else {
        echo '<a class="button" href="./?p=Administrator&f=contactlist&archived=true">Voir les messages archivés</a>';
        $tab = ModelContact::getAllByAttribute("archived", "0");
    }
    if ($tab != false) {
        foreach ($tab as $v) {
            require('view/admin/contactcard.php');
        }
    } else {
        echo '<h1>Aucun message trouvé</h1>';
    }
    ?>
</div>
