<div class="connect-container">
    <h1>Administration</h1>
    <hr />
    <div class="connect-form">
        <form class="center" action="./?p=Administrator&f=login" method="post">
            <div class="field">
                <label>Password</label>
                <input type="password" name="password" class="form-control">
            </div>
            <div class="field">
                <input type="submit" class="button" value="Login">
            </div>
        </form>
    </div>
</div>