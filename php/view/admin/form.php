<div class="formulaire">
    <div class="container">
        <div class="titleform">
            <?php
            if($v !== null) {
                echo "<h1>Modifier la création " . $v->getId() . "</h1>";
            } else {
                echo "<h1>Ajouter une création</h1>";
            }
            ?>
            <hr/>
        </div>
        <form method="post" action="<?php
        if($v !== null)
            echo './?p=Administrator&f=updated&id=' . $v->getId();
        else
            echo './?p=Administrator&f=created';
        ?>" enctype="multipart/form-data">

            <div class="input-group">
                <label>Nom</label>
                <input type="text" placeholder="Entrez le nom de la création" class="input-box" name="name" value="<?php
                if($v !== null){
                    echo $v->getName();
                }
                ?>" required>
            </div>

            <div class="input-group">
                <label>Date</label>
                <input type="date" placeholder="Entrez la date de la création" class="input-box" name="date" value="<?php
                if($v !== null){
                    echo $v->getDate();
                }
                ?>" required>
            </div>

            <div class="input-group">
                <label>Description</label>
                <textarea placeholder="Entrez la description de la création" class="input-box desc" name="description" required><?php
                    if($v !== null){
                        echo $v->getDescription();
                    }
                    ?></textarea>
            </div>

            <div class="input-group center">
                <label for="file" class="label-file">Télécharger les images</label>
                <input id="file" class="input-file" name="upload[]" type="file" multiple <?php if($v == null) echo "require"?>/>
            </div>

            <div class="input-group">
                <button type="submit" name="submit" class="submit-btn"><?php
                    if($v !== null) {
                        echo "Modifier";
                    } else {
                        echo "Créer";
                    }
                    ?></button>
            </div>

        </form>
    </div>
</div>