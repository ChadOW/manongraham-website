<div class="card">
    <div class="top">
        <h2>ID: <?php echo $v->getId() ?></h2>
        <h1>Nom: <?php echo $v->getName() ?></h1>
        <h2>Date: <?php echo $v->getDate() ?></h2>
        <h2>Description: <?php echo $v->getDescription() ?></h2>
    </div>
    <?php echo $v->getFirstFileHTML()?>
    <div class="center">
        <div class="inline center">
            <a href="./?p=Visitor&f=see&id=<?php echo $v->getId() ?>"><i class="material-icons">folder</i></a>
            <a href="./?p=Administrator&f=form&id=<?php echo $v->getId() ?>"><i class="material-icons">edit</i></a>
            <a href="./?p=Administrator&f=visible&id=<?php echo $v->getId() ?>"><i class="material-icons"<?php echo visibleStyle($v) ?>>remove_red_eye</i></a>
            <a href="./?p=Administrator&f=favorite&id=<?php echo $v->getId() ?>"><i class="material-icons"<?php echo favoriteStyle($v) ?>>favorite</i></a>
        </div>
        <a class="button red" href="./?p=Administrator&f=delete&id=<?php echo $v->getId() ?>">Supprimer</a>
    </div>
</div>