<div class="inline center">
    <a class="button" href="./?p=Administrator&f=form">Ajouter une création</a>
    <a class="button" href="./?p=Administrator&f=contactlist&archived=false">Voir les contacts</a>
</div>
<div class="center">
    <a class="button red" href="./?p=Administrator&f=disconnect">Se déconnecter</a>
</div>

<div class="center">
    <div class="admin list center">
        <?php
        require_once('model/ModelCreation.php');

        function visibleStyle($v)
        {
            if ($v->getIsVisible() == 0)
                return ' style="color: red;"';
            return ' style="color: rgb(150, 150, 150);"';
        }

        function favoriteStyle($v)
        {
            if ($v->getIsFavorite() == 1)
                return ' style="color: yellow;"';
            return ' style="color: rgb(150, 150, 150);"';
        }

        foreach (ModelCreation::getAll() as $v) {
            require('view/admin/creacard.php');
        }
        ?>
    </div>
</div>
