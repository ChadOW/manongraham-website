<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Manon Graham Fonseca</title>
        <link rel="stylesheet" href="assets/css/visitor/carousel.css" type="text/css">
        <link rel="stylesheet" href="assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="assets/css/header.css" type="text/css">
        <link rel="stylesheet" href="assets/css/content.css" type="text/css">
        <link rel="stylesheet" href="assets/css/footer.css" type="text/css">
        <link rel="stylesheet" href="assets/css/error.css" type="text/css">
        <link rel="stylesheet" href="assets/css/file.css" type="text/css">

        <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <link rel="stylesheet" href="assets/css/visitor/list.css" type="text/css">
        <link rel="stylesheet" href="assets/css/visitor/see.css" type="text/css">
        <link rel="stylesheet" href="assets/css/visitor/home.css" type="text/css">
        <link rel="stylesheet" href="assets/css/visitor/about.css" type="text/css">

        <link rel="stylesheet" href="assets/css/admin/connect.css" type="text/css">
        <link rel="stylesheet" href="assets/css/admin/form.css" type="text/css">
        <link rel="stylesheet" href="assets/css/admin/contact.css" type="text/css">

        <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <!-- <script src="assets/js/homescroll.js"></script> -->

        
        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-YW7WQTZ7JG"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-YW7WQTZ7JG');
        </script>

    </head>
    <body>

        <header>
            <a class="inline center" href="./">
                <img src="assets/img/home.png" alt="home"/>
                <h1>Manon GRAHAM FONSECA</h1>
            </a>
            <div class="pages">
                <a href="./?p=Visitor&f=about"><h2>BIOGRAPHY</h2></a>
                <a href="./?p=Visitor&f=portfolio"><h2>PORTFOLIO</h2></a>
                <a href="./?p=Visitor&f=contact"><h2>CONTACT</h2></a>
            </div>
        </header>

        <div class="content">
            <?php
            // Si $controleur='voiture' et $view='list',
            // alors $filepath="/chemin_du_site/view/voiture/list.php"
            function build_path(array $array)
            {
                return $array[0] . "/" . $array[1] . "/" . $array[2]. ".php";
            }

            $filepath = build_path(array("view", $controller, $view));
            require $filepath;
            ?>
        </div>

        <footer>
            <div></div>
            <p>Developped by <a href="https://chades.fr">Chad Estoup--Streiff</a></p>
            <a href="./?p=Administrator"><img src="./assets/img/rouage.png" alt="admin"/></a>
        </footer>
    </body>
</html>
<?php die;