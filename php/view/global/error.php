<div class="center">
    <div class="error">
        <p>
            An error occurred.
            <?php
            if (array_key_exists("error_message", $_POST))
                echo "Message: " . $_POST["error_message"];
            else
                echo "Contactez l'Administrateur."
            ?>
        </p>
        <a class="button" href="./">Back to home</a>
    </div>
</div>
