<div class="pane anchor">
    <div class="center"><h1>My favorites creations</h1></div>
    <div class="carousel" data-flickity='{ "imagesLoaded": true, "percentPosition": false, "autoPlay": 3000 }'>
        <?php
        require_once('model/ModelCreation.php');
        foreach (ModelCreation::getAllByAttribute('isfavorite', '1') as $v) {
            if ($v->getIsVisible() == 1)
                echo '  <div class="carousel-cell">
                                <a href="./?p=Visitor&f=see&id=' . $v->getId() . '">' . $v->getFirstFileHTML() . '</a>
                            </div>';
        }
        ?>

    </div>
</div>
<div class="pane anchor center">
    <h1>DISCOVER ME</h1>
    <a href="./?p=Visitor&f=about" class="button">Click here</a>
</div>
<div class="pane anchor center">
    <h1>SEE MY CREATIONS</h1>
    <a href="./?p=Visitor&f=portfolio" class="button">Click here</a>
</div>
<div class="pane anchor center">
    <h1>CONTACT ME</h1>
    <a href="./?p=Visitor&f=contact" class="button">Click here</a>
</div>