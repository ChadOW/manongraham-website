var delay = false;

function scrollToAnchor(event) {
    let i;
    let t;
    if (event != null && event.cancelable)
        event.preventDefault();
    if (delay) return;

    delay = true;
    setTimeout(function () {
        delay = false
    }, 200)

    let wd = 0;
    if (event != null)
        wd = event.originalEvent.wheelDelta || -event.originalEvent.detail;

    const a = document.getElementsByClassName('anchor');
    if (wd < 0) {
        for (i = 0; i < a.length; i++) {
            t = a[i].getClientRects()[0].top;
            if (t >= 40) break;
        }
    } else {
        for (i = a.length - 1; i >= 0; i--) {
            t = a[i].getClientRects()[0].top;
            if (t < -20) break;
        }
    }

    if (i <= 0)
        i = 0;
    if (i >= 0 && i < a.length) {
        $('html,body').animate({
            scrollTop: a[i].offsetTop
        });
    }
    return undefined;
}

window.addEventListener("load", function(event) {
    if (document.getElementsByClassName('anchor').length > 0 &&  !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
        scrollToAnchor(null);
        $(document).on('mousewheel DOMMouseScroll', scrollToAnchor);
    }
});