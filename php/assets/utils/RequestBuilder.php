<?php
    require_once('assets/utils/DBCom.php');
    class RequestBuilder {
        private $table;
        private $selectors;
        private $key;
        private $page;
        private $pageSize = 20;
        private $conditions;
        private $order;

        public function __construct($table, $selector)
        {
            $this->table = $table;
            $this->selectors = $selector;
            $this->conditions = array();
        }

        public function setPage($page)
        {
            $this->page = $page;
        }

        public function setPageSize($pageSize)
        {
            $this->pageSize = $pageSize;
        }

        public function setCondition($attribute, $value, $deleteSelector) {
            if (in_array($attribute, $this->selectors)) {
                $this->conditions[$attribute] = $value;
                if ($deleteSelector === true)
                    unset($this->selectors[array_search($attribute, $this->selectors)]);
            }
        }

        public function search($key) {
            $this->key = $key;
        }

        public function orderBy($order) {
            $this->order = $order;
        }

        public function launch()
        {
            $sql = 'SELECT ' . $this->createSelectorString() . ' FROM ' . $this->table . ' ' . $this->createWhereString() . ' ' . $this->createOrderString() . ' ' . $this->createPageString() . ';';
            //var_dump($sql);
            //var_dump($this);
            try {
                $req_prep = DBCom::getPDO()->prepare($sql);
                $tab = array();
                if (!is_null($this->key))
                    $tab['key'] = "%$this->key%";
                if (sizeof($this->conditions) > 0) {
                    foreach ($this->conditions as $key => $value) {
                        $tab['cond_' . $key] = $value;
                    }
                }
                $req_prep->execute($tab);
                return $req_prep;
            } catch (PDOException $e) {
                require_once('assets/utils/CustomError.php');
                CustomError::callError($e->getMessage());
                return false;
            }
        }

        private function createSelectorString()
        {
            if (sizeof($this->selectors) > 0) {
                $str = '';
                foreach ($this->selectors as $value) {
                    $str = $str . ', ' . $value;
                }
                return substr($str, 2);
            } else {
                return '';
            }
        }

        private function createWhereString()
        {
            $str = 'WHERE ';
            if (sizeof($this->conditions) > 0) {
                foreach ($this->conditions as $key => $value) {
                    $str = $str . $key . ' = :cond_' . $key . ' AND ';
                }
            }
            return $str . $this->createSearchString();
        }

        private function createSearchString()
        {
            if (!is_null($this->key)) {
                $str = '';
                foreach ($this->selectors as $value) {
                    $str = $str . $value . ' LIKE :key OR ';
                }
                return substr($str, 0, strlen($str) -4);
            }
            return 'TRUE';
        }

        private function createPageString()
        {
            if (!is_null($this->page)) {
                return 'LIMIT ' . ($this->page * $this->pageSize) . ', ' . $this->pageSize;
            }
            return '';
        }

        private function createOrderString()
        {
            if (!is_null($this->order)) {
                return 'ORDER BY ' . $this->order;
            }
            return '';
        }


        public function fetchAndGSONEncode() {
            $rs = $this->launch();
            $rs = $rs->fetchAll(PDO::FETCH_ASSOC);
            $arr = array();
            foreach ($rs as $v) {
                $arr[] = json_encode($v);
            }
            return json_encode($arr);
        }

        public function fetch($model)
        {
            $rs = $this->launch();
            if ($model == null) {
                $tab = $rs->fetchAll(PDO::FETCH_OBJ);
            } else {
                $tab = $rs->fetchAll(PDO::FETCH_CLASS, $model);
            }
            if(empty($tab))
                return false;
            return $tab;
        }
    }