<?php
    class User {

        public static function isConnected() {
            return isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true;
        }

        public static function disconnect() {
            if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
                $_SESSION = array();
                session_destroy();
            }

            header("location: ./");
        }

        public static function connect()
        {
            // Check if password is empty
            if (array_key_exists("password", $_POST)) {
                $password = $_POST["password"];

                
                require_once('/var/www/config/GConf.php');
                if (password_verify($password, GConf::getHashedAdminMdp())) {
                    $_SESSION["loggedin"] = true;
                } else {
                    require_once("assets/utils/CustomError.php");
                    CustomError::callError("Mot de passe incorect");
                }
            }
            header("location: ./?p=Administrator");
        }
    }