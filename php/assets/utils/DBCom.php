<?php
    class DBCom {
        public static $pdo = NULL;

        public static function init() {
            require_once('/var/www/config/DBConf.php');
            
            try {
                self::$pdo = new PDO( "mysql:host=" . DBConf::getHostname() . ";dbname=" . DBConf::getDatabase() . ";port=" . DBConf::getPort(), DBConf::getLogin(), DBConf::getPassword());
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::create_tables();
            } catch(PDOException $e) {
                require_once("assets/utils/CustomError.php");
                CustomError::callError("Erreur lors de la connection à la Base de Données: " . $e);
            }
        }

        public static function create_tables() {
            try {
                self::getPDO()->query("
                CREATE TABLE IF NOT EXISTS `Creation` (
                `id` int(11) NOT NULL,
                `name` varchar(50) NOT NULL,
                `description` text DEFAULT NULL,
                `date` date NOT NULL,
                `isfavorite` tinyint(1) NOT NULL,
                `isvisible` tinyint(1) NOT NULL,
                `files` text NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;    
                
                ALTER TABLE `Creation`
                ADD PRIMARY KEY (`id`);
                
                ALTER TABLE `Creation`
                MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;  
                COMMIT;");

                self::getPDO()->query("
                CREATE TABLE IF NOT EXISTS `Contact` (
                `id` int(11) NOT NULL,
                `name` varchar(50) NOT NULL,
                `mail` varchar(50) NOT NULL,
                `object` text NOT NULL,
                `message` text NOT NULL,
                `date` date NOT NULL,
                `archived` boolean NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                
                ALTER TABLE `Contact`
                ADD PRIMARY KEY (`id`);
                
                ALTER TABLE `Contact`
                MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
                COMMIT;");
            } catch(PDOException $e) {
                require_once("assets/utils/CustomError.php");
                CustomError::callError("Erreur lors de la connection à la Base de Données: " . $e);
            }
        }



        public static function getPDO() {
            if (is_null(self::$pdo))
                self::init();
            return self::$pdo;
        }
    }
