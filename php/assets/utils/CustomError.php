<?php
    class CustomError {
        public static function callError($message = NULL) {
            
            if (!is_null($message)) {
                $_POST["error_message"] = $message;
            }
            $controller = 'global';
            $view = 'error';
            require_once('view/view.php');
        }
    }